package com.acs.rizky.gileague.presenter

import com.acs.rizky.gileague.api_response.LeagueListResponse
import com.acs.rizky.gileague.api_response.ScheduleResponse
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.acs.rizky.gileague.view.ScheduleView
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class SchedulesPresenter(private val view: ScheduleView,
                         private val apiRespository: ApiRepository,
                         private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getScheduleList(league: String?, id: String?){
        view.showLoading()
        async(context.main) {
            val data = bg {
                gson.fromJson(apiRespository
                        .doRequest(SportDBApi.getSchedule(league, id)),
                        ScheduleResponse::class.java
                )
            }
            view.showScheduleList(data.await().events)
            view.hideLoading()

        }
    }
}