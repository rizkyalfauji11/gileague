package com.acs.rizky.gileague.fragment


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.*

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.array.league
import com.acs.rizky.gileague.R.color.colorAccent
import com.acs.rizky.gileague.R.color.whiteSoSmoked
import com.acs.rizky.gileague.R.string.idOnly
import com.acs.rizky.gileague.TeamDetailActivity
import com.acs.rizky.gileague.adapter.TeamsAdapter
import com.acs.rizky.gileague.model.ScheduleModel
import com.acs.rizky.gileague.model.Team
import com.acs.rizky.gileague.presenter.SchedulesPresenter
import com.acs.rizky.gileague.presenter.TeamsPresenter
import com.acs.rizky.gileague.utilities.*
import com.acs.rizky.gileague.view.ScheduleView
import com.acs.rizky.gileague.view.TeamView
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class TeamsFragment : Fragment(), AnkoComponent<Context>, TeamView {

    private var teams: MutableList<Team> = mutableListOf()
    private lateinit var presenter: TeamsPresenter
    private lateinit var adapter: TeamsAdapter
    private lateinit var spinner: Spinner
    private lateinit var listEvent: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var leagueName: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return createView(AnkoContext.create(ctx))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        val spinnerItems = resources.getStringArray(league)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter

        adapter = TeamsAdapter(teams) {
            ctx.startActivity<TeamDetailActivity>(resources.getString(idOnly) to "${it.teamId}")
        }
        listEvent.layoutManager = LinearLayoutManager(ctx)
        listEvent.addItemDecoration(DividerItemDecoration(ctx))
        listEvent.adapter = adapter
        listEvent.id = R.id.list_event

        val request = ApiRepository()
        val gson = Gson()
        presenter = TeamsPresenter(this, request, gson)
        spinner.id = R.id.spinner
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                leagueName = spinner.selectedItem.toString()
                EspressoIdlingResource.increment()
                presenter.getTeamList(leagueName)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}

        }



        swipeRefresh.onRefresh {
            EspressoIdlingResource.increment()
            presenter.getTeamList(leagueName)
        }

    }

    @SuppressLint("NewApi")
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main_menu, menu)
        val searchItem = menu?.findItem(R.id.item_search)
        val searchView = SearchView(ctx)
        searchView.queryHint = "Search Team"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val dataFilter: MutableList<Team> = mutableListOf()
                val myNewText = newText?.toLowerCase()
                println(myNewText)
                for (data: Team in teams) {
                    val homeTeam = data.teamName?.toLowerCase()
                    if (homeTeam?.contains(myNewText.toString())!! ) {
                        dataFilter.add(data)
                    }

                    adapter.setFilter(dataFilter)
                }
                return true
            }
        })
        searchItem?.setActionView(searchView)
    }

    override fun showTeamList(data: List<Team>) {
        swipeRefresh.isRefreshing = false
        teams.clear()
        teams.addAll(data)
        adapter.notifyDataSetChanged()
        EspressoIdlingResource.decrement()
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams (width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)
            backgroundColor = resources.getColor(whiteSoSmoked)

            spinner = spinner ()
            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout{
                    lparams (width = matchParent, height = wrapContent)

                    listEvent = recyclerView {
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                        id = R.id.list_team
                    }

                    progressBar = progressBar {
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }


}
