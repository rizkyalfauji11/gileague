package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.ScheduleModel

data class ScheduleResponse(
        val events: List<ScheduleModel>
)

