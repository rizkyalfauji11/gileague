package com.acs.rizky.gileague.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.string.next
import com.acs.rizky.gileague.R.string.past
import com.acs.rizky.gileague.adapter.TabMatchAdapter
import kotlinx.android.synthetic.main.fragment_tab_match.*
import org.jetbrains.anko.support.v4.ctx


class TabMatchFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_tab_match, container, false)
        return view

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        addTab()


    }

    private fun addTab(){
        val tabMatchAdapter = TabMatchAdapter(childFragmentManager)
        tabMatchAdapter.add(MatchFragment.newInstance(resources.getString(R.string.event_past_league), resources.getString(R.string.id_league)), resources.getString(past))
        tabMatchAdapter.add(NextMatchFragment.newInstance(resources.getString(R.string.event_next_league), resources.getString(R.string.id_league)), resources.getString(next))
        viewpager.adapter = tabMatchAdapter
        tabs.setupWithViewPager(viewpager)
    }

}
