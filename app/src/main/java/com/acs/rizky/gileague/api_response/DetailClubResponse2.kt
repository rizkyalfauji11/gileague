package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.DetailClubModel2

data class DetailClubResponse2(
        val teams: List<DetailClubModel2>
)