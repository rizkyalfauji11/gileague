package com.acs.rizky.gileague.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.color.colorPrimary
import com.acs.rizky.gileague.R.drawable.ic_notification
import com.acs.rizky.gileague.R.id.*
import com.acs.rizky.gileague.R.string.versus
import com.acs.rizky.gileague.model.ScheduleModel
import com.acs.rizky.gileague.utilities.MyUtilsNew
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import com.acs.rizky.gileague.MainActivity
import android.text.method.TextKeyListener.clear
import android.widget.*
import kotlinx.android.synthetic.main.activity_detail.view.*
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*
import kotlin.collections.ArrayList


class ScheduleAdapter(private var schedules: MutableList<ScheduleModel>, private val listener: (ScheduleModel) -> Unit)
    : RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder>(){
    private val filter: MutableList<ScheduleModel> = mutableListOf<ScheduleModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        return ScheduleViewHolder(ScheduleUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = schedules.size

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
        holder.bindItem(schedules[position], listener)
    }

    class ScheduleViewHolder(view: View) : RecyclerView.ViewHolder(view){
        private val homeName: TextView = view.find(home_name)
        private val awayName: TextView = view.find(away_name)
        private val dateEvent: TextView = view.findViewById(date_event)
        private val homeScore: TextView = view.findViewById(home_score)
        private val awayScore: TextView = view.findViewById(away_score)
        private val timeEvent: TextView = view.findViewById(clock)
        private val imageRemind: ImageView = view.findViewById(imageReminder)

        fun bindItem(schedules: ScheduleModel, listener: (ScheduleModel) -> Unit){
            homeName.text = schedules.strHomeTeam
            awayName.text = schedules.strAwayTeam
            dateEvent.text = MyUtilsNew().stringToDate(schedules.dateEvent)
            timeEvent.text = schedules.strTime?.substring(0,5)
            homeScore.text = schedules.intHomeScore
            awayScore.text = schedules.intAwayScore
                imageRemind.visibility = View.INVISIBLE

            itemView.setOnClickListener {
                listener(schedules)
            }

        }


    }



    fun setFilter(filterList: MutableList<ScheduleModel>) {
        schedules = mutableListOf()
        schedules.addAll(filterList)
        notifyDataSetChanged()
    }

    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())
        filter.clear()
        if (charText.length == 0) {
            schedules.addAll(filter)
        } else {
            for (wp in filter) {
                if (wp.strHomeTeam?.toLowerCase(Locale.getDefault())?.contains(charText)!!) {
                    schedules.add(wp)
                }
            }
        }
        notifyDataSetChanged()
    }

    class ScheduleUI : AnkoComponent<ViewGroup>{
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                    cardView {
                        lparams(width = matchParent)
                        setContentPadding(16, 16, 16, 16)
                        cardElevation = 0F

                        verticalLayout {
                            lparams(width = matchParent, height = wrapContent)

                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                textView {
                                    gravity = Gravity.CENTER_HORIZONTAL
                                    id = R.id.date_event
                                    textSize = 14F
                                    textColor = resources.getColor(colorPrimary)
                                }.lparams(width = matchParent, height = wrapContent, weight = 1F){
                                    leftMargin = dip(20)
                                }

                                imageView {
                                    id = R.id.imageReminder
                                    setImageDrawable(resources.getDrawable(ic_notification))
                                    setColorFilter(ContextCompat.getColor(context, colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY)
                                }.lparams(height = dip(20), width = dip(20)){
                                }
                            }

                            textView{
                                gravity = Gravity.CENTER_HORIZONTAL
                                id = R.id.clock
                                textSize = 13F
                                textColor = resources.getColor(colorPrimary)
                            }.lparams(width = matchParent){
                                padding = dip(2)
                            }
                            linearLayout {
                                orientation = LinearLayout.HORIZONTAL
                                setPadding(0, 8, 0, 4)
                                textView {
                                    id = R.id.home_name
                                    textSize = 16F


                                }.lparams {
                                    width = dip(0)
                                    weight = 1F
                                    height = wrapContent

                                }
                                textView {
                                    id = R.id.home_score
                                    textSize = 18F
                                }.lparams {
                                    rightMargin = dip(8)
                                    leftMargin = dip(8)
                                }
                                textView {
                                    text = resources.getString(versus)
                                    textSize = 16F


                                }.lparams(width = wrapContent, height = wrapContent)

                                textView {
                                    id = R.id.away_score
                                    textSize = 18F
                                }.lparams {
                                    rightMargin = dip(8)
                                    leftMargin = dip(8)
                                }
                                textView {
                                    id = R.id.away_name
                                    gravity = Gravity.END
                                    textSize = 16F
                                }.lparams {
                                    width = dip(0)
                                    weight = 1F
                                    height = wrapContent
                                }
                            }
                        }
                    }
                }
            }
        }

    }
