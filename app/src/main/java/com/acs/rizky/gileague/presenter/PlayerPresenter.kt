package com.acs.rizky.gileague.presenter

import com.acs.rizky.gileague.api_response.DetailResponse
import com.acs.rizky.gileague.api_response.PlayerListResponse
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.acs.rizky.gileague.view.PlayerView
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerPresenter(private val view: PlayerView,
                      private val api: ApiRepository,
                      private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()){

    fun getPlayers(id: String?){
        async(context.main) {
            val data = bg {
                gson.fromJson(api
                        .doRequest(SportDBApi.getPlayerList(id)),
                        PlayerListResponse::class.java
                )
            }
            view.showListPlayer(data.await().player)
        }
    }
}