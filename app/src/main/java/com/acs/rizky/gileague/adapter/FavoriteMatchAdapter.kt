package com.acs.rizky.gileague.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.id.clock
import com.acs.rizky.gileague.mydb.FavoriteMatch
import com.acs.rizky.gileague.utilities.MyUtilsNew
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class FavoriteMatchAdapter (private val favorite: List<FavoriteMatch>, private val listener: (FavoriteMatch) -> Unit)
    : RecyclerView.Adapter<FavoriteMatchAdapter.FavoriteMatchViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteMatchViewHolder {
        return FavoriteMatchViewHolder(MatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: FavoriteMatchViewHolder, position: Int) {
        holder.bindItem(favorite[position], listener)
    }

    class FavoriteMatchViewHolder(view: View) : RecyclerView.ViewHolder(view){
        private val homeName: TextView = view.find(R.id.home_name)
        private val awayName: TextView = view.find(R.id.away_name)
        private val dateEvent: TextView = view.findViewById(R.id.date_event)
        private val homeScore: TextView = view.findViewById(R.id.home_score)
        private val awayScore: TextView = view.findViewById(R.id.away_score)
        private val timeEvent: TextView = view.findViewById(clock)

        fun bindItem(favorite: FavoriteMatch, listener: (FavoriteMatch) -> Unit){
            homeName.text = favorite.homeTeam
            awayName.text = favorite.awayTeam
            dateEvent.text = MyUtilsNew().stringToDate(favorite.eventDate)
            homeScore.text = favorite.homeScore
            awayScore.text = favorite.awayScore
            timeEvent.text = favorite.eventTime?.substring(0,5)
            itemView.setOnClickListener {
                listener(favorite)
            }
        }
    }

}

class MatchUI() : AnkoComponent<ViewGroup>{
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){
            cardView {
                lparams(width = matchParent)
                setContentPadding(16, 16, 16, 16)
                cardElevation = 0F

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)

                    textView {
                        gravity = Gravity.CENTER_HORIZONTAL
                        id = R.id.date_event
                        textSize = 14F
                        textColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = matchParent, height = wrapContent)

                    textView{
                        gravity = Gravity.CENTER_HORIZONTAL
                        id = R.id.clock
                        textSize = 13F
                        textColor = resources.getColor(R.color.colorPrimary)
                    }.lparams(width = matchParent){
                        padding = dip(2)
                    }
                    linearLayout {
                        orientation = LinearLayout.HORIZONTAL
                        setPadding(0, 8, 0, 4)
                        textView {
                            id = R.id.home_name
                            textSize = 14F


                        }.lparams {
                            width = dip(0)
                            weight = 1F
                            height = wrapContent

                        }
                        textView {
                            id = R.id.home_score
                            textSize = 14F
                        }.lparams {
                            rightMargin = dip(8)
                            leftMargin = dip(8)
                        }
                        textView {
                            text = "vs"
                            textSize = 14F


                        }.lparams(width = wrapContent, height = wrapContent)

                        textView {
                            id = R.id.away_score
                            textSize = 14F
                        }.lparams {
                            rightMargin = dip(8)
                            leftMargin = dip(8)
                        }
                        textView {
                            id = R.id.away_name
                            gravity = Gravity.END
                            textSize = 14F
                        }.lparams {
                            width = dip(0)
                            weight = 1F
                            height = wrapContent
                        }
                    }
                }
            }
        }
    }

}