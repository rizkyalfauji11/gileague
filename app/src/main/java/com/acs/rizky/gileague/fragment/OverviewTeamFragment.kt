package com.acs.rizky.gileague.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.model.Team
import com.acs.rizky.gileague.view.TeamDetailView
import kotlinx.android.synthetic.main.fragment_overview_team.*


class OverviewTeamFragment : Fragment() {

    companion object {
        fun newInstance(text: String?) : OverviewTeamFragment{
            val fragment = OverviewTeamFragment()
            val args = Bundle()
            args.putString("overview", text)
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_overview_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        text_overview.text = arguments?.getString("overview")
        println("Naha teu nepi kadieu")
    }

}
