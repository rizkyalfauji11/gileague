package com.acs.rizky.gileague.fragment



import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acs.rizky.gileague.DetailActivity

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.color.whiteSoSmoked
import com.acs.rizky.gileague.R.string.id_event
import com.acs.rizky.gileague.TeamDetailActivity
import com.acs.rizky.gileague.adapter.FavoriteMatchAdapter
import com.acs.rizky.gileague.adapter.FavoriteTeamsAdapter
import com.acs.rizky.gileague.mydb.Favorite
import com.acs.rizky.gileague.mydb.FavoriteMatch
import com.acs.rizky.gileague.mydb.database
import com.acs.rizky.gileague.utilities.DividerItemDecoration
import com.acs.rizky.gileague.utilities.EspressoIdlingResource
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout


class FavoriteMatchFragment : Fragment(), AnkoComponent<Context> {
    companion object {
        fun newInstance() : FavoriteMatchFragment{
            val fragment = FavoriteMatchFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }

    private var favorites: MutableList<FavoriteMatch> = mutableListOf()
    private lateinit var adapter: FavoriteMatchAdapter
    private lateinit var listEvent: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        linearLayout {
            lparams(height = matchParent, width = matchParent)
            backgroundColor = resources.getColor(whiteSoSmoked)
            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)
                id = R.id.swipeRefresh2

                listEvent = recyclerView {
                    lparams (width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                    id = R.id.rv_1

                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        EspressoIdlingResource.increment()
        adapter = FavoriteMatchAdapter(favorites){
            ctx.startActivity<DetailActivity>(resources.getString(id_event) to "${it.idEvent}")
        }
        listEvent.layoutManager = LinearLayoutManager(ctx)
        listEvent.addItemDecoration(DividerItemDecoration(ctx))
        listEvent.adapter = adapter
        showFavorite()
        swipeRefresh.onRefresh {
            EspressoIdlingResource.increment()
            favorites.clear()
            showFavorite()
        }
    }
    private fun showFavorite(){
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(FavoriteMatch.TB_EVENT_FAVORITE)
            val favorite = result.parseList(classParser<FavoriteMatch>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
            EspressoIdlingResource.decrement()
        }
    }

}
