package com.acs.rizky.gileague.model

import com.google.gson.annotations.SerializedName

data class DetailClubModel2(
        @SerializedName("strTeamBadge")
        var strTeamBadge: String? = null
)