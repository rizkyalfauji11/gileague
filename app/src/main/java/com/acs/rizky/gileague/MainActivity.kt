package com.acs.rizky.gileague


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import com.acs.rizky.gileague.R.id.final_match
import com.acs.rizky.gileague.R.id.item_search
import com.acs.rizky.gileague.R.string.*
import com.acs.rizky.gileague.adapter.ScheduleAdapter
import com.acs.rizky.gileague.fragment.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(mainToolbar)

        navigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigationView.selectedItemId = final_match

    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId){
            R.id.final_match -> {
                //iconVisibility(false)
                mainToolbar?.title = resources.getString(match)
                createFragment(resources.getString(event_past_league), resources.getString(id_league), 3)
                return@OnNavigationItemSelectedListener true
            }
            R.id.next_match -> {
                //iconVisibility(true)
                mainToolbar?.title = resources.getString(footbal_apps)
                createFragment(resources.getString(event_next_league), resources.getString(id_league), 4)
                return@OnNavigationItemSelectedListener true
            }

            R.id.favorites -> {
                //iconVisibility(false)
                mainToolbar?.title = resources.getString(fav)
                createFragment("", "", 5)
                return@OnNavigationItemSelectedListener true
            }
        }

        false
    }
    private fun createFragment(league: String, id:String, idFragment: Int){
        var fragment: Fragment? = null
        when(idFragment){
            1 -> {
                fragment = MatchFragment.newInstance(league, id)
            }

            2 ->{
                fragment = FavoriteMatchFragment.newInstance()
            }
            3 -> {
                fragment = TabMatchFragment()
            }
            4 -> {
                fragment = TeamsFragment()
            }
            5 -> {
                fragment = TabFavoriteFragment()
            }
        }

        if (fragment != null) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_container, fragment, fragment::class.java.simpleName)
                    .commit()
        }
    }


}
