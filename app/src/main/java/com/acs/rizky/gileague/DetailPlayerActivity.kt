package com.acs.rizky.gileague

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.acs.rizky.gileague.model.DetailPlayerModel
import com.acs.rizky.gileague.presenter.PlayerDetailPresenter
import com.acs.rizky.gileague.presenter.PlayerPresenter
import com.acs.rizky.gileague.utilities.*
import com.acs.rizky.gileague.view.DetailPlayerView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_player.*

class DetailPlayerActivity : AppCompatActivity(), DetailPlayerView {
    private lateinit var idPlayer: String
    private lateinit var presenter: PlayerDetailPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_player)
        idPlayer = intent.getStringExtra("id")

        presenter = PlayerDetailPresenter(this, ApiRepository(), Gson())
        presenter.getDetailPlayer(idPlayer)
        EspressoIdlingResource.increment()
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun showDetailPlayer(data: List<DetailPlayerModel>) {
        Picasso.get().load(data[0].strFanart2).into(image_player)
        weight.text = data[0].strWeight
        height.text = data[0].strHeight
        position.text = data[0].strPosition
        desc.text = data[0].strDescriptionEN
        setSupportActionBar(player_toolbar)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        player_toolbar.title = data[0].strPlayer
        EspressoIdlingResource.decrement()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            super.onBackPressed()
        }
        when(item.itemId){
            android.R.id.home ->{
                super.onBackPressed()
            }

        }
        return super.onOptionsItemSelected(item)
    }

}
