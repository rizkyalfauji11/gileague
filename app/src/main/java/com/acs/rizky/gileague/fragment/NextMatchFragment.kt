package com.acs.rizky.gileague.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SearchView
import com.acs.rizky.gileague.DetailActivity

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.adapter.ScheduleAdapter
import com.acs.rizky.gileague.model.ScheduleModel
import com.acs.rizky.gileague.presenter.SchedulesPresenter
import com.acs.rizky.gileague.utilities.*
import com.acs.rizky.gileague.view.ScheduleView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_next_match.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx

class NextMatchFragment : Fragment(), ScheduleView {
    companion object {
        fun newInstance(league: String, id: String) : NextMatchFragment{
            val fragment = NextMatchFragment()
            val args = Bundle()
            args.putString("league", league)
            args.putString("id", id)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var presenter: SchedulesPresenter
    private lateinit var adapter: ScheduleAdapter
    private var schedules: MutableList<ScheduleModel> = mutableListOf()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_next_match, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        val args = arguments
        val spinnerItems = resources.getStringArray(R.array.league)
        val idOfSpinner = resources.getIntArray(R.array.league_id)

        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        match_spinner.adapter = spinnerAdapter


        adapter = ScheduleAdapter(schedules){
            ctx.startActivity<DetailActivity>(resources.getString(R.string.id_event) to it.idEvent)
        }


        rv_2.layoutManager = LinearLayoutManager(ctx)
        rv_2.addItemDecoration(DividerItemDecoration(ctx))
        rv_2.adapter = adapter

        match_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                showDataNow(args?.getString(resources.getString(R.string.leagueOnly)).toString(), idOfSpinner[position].toString())
            }

        }

    }

    @SuppressLint("NewApi")
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.main_menu, menu)
        val searchItem = menu?.findItem(R.id.item_search)
        val searchView = SearchView(ctx)
        searchView.queryHint = "Search Event"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val dataFilter: MutableList<ScheduleModel> = mutableListOf()
                val myNewText = newText?.toLowerCase()
                println(myNewText)
                for (data: ScheduleModel in schedules) {
                    val homeTeam = data.strHomeTeam?.toLowerCase()
                    val awayTeam = data.strAwayTeam?.toLowerCase()
                    if (homeTeam?.contains(myNewText.toString())!! || awayTeam?.contains(myNewText.toString())!!) {
                        dataFilter.add(data)
                    }

                    adapter.setFilter(dataFilter)
                }
                return true
            }
        })
        searchItem?.setActionView(searchView)
    }


    private fun showDataNow(event: String, id: String) {
        EspressoIdlingResource.increment()
        val request = ApiRepository()
        val gson = Gson()
        presenter = SchedulesPresenter(this, request, gson)

        presenter.getScheduleList(event, id)

    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        try {
            progressBar.invisible()
        }catch (e: Exception){
            e.printStackTrace()
        }

    }

    override fun showScheduleList(data: List<ScheduleModel>) {
        schedules.clear()
        schedules.addAll(data)
        adapter.notifyDataSetChanged()
        EspressoIdlingResource.decrement()
    }



}
