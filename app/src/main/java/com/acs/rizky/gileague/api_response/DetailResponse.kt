package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.DetailEventModel

data class DetailResponse(
        val events: List<DetailEventModel>
)