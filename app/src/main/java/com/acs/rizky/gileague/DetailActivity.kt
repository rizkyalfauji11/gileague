package com.acs.rizky.gileague

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.ActionBar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.acs.rizky.gileague.R.id.add_to_favorite
import com.acs.rizky.gileague.R.string.*
import com.acs.rizky.gileague.model.DetailClubModel1
import com.acs.rizky.gileague.model.DetailClubModel2
import com.acs.rizky.gileague.model.DetailEventModel
import com.acs.rizky.gileague.mydb.FavoriteMatch
import com.acs.rizky.gileague.mydb.database
import com.acs.rizky.gileague.view.DetailView
import com.acs.rizky.gileague.presenter.DetailPresenter
import com.acs.rizky.gileague.utilities.*
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar

class DetailActivity : AppCompatActivity(), DetailView {
    override fun showDetailClub2(data: List<DetailClubModel2>) {
        Picasso.get().load(data[0].strTeamBadge).into(away_logo)
    }

    override fun showDetailClub1(data: List<DetailClubModel1>) {
        Picasso.get().load(data[0].strTeamBadge).into(home_logo)
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    private lateinit var presenter: DetailPresenter
    lateinit var toolbar: ActionBar
    lateinit var detailEventModel: DetailEventModel

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    private lateinit var idEvent: String

    override fun showDetailView(data: List<DetailEventModel>) {
        txt_date.text = MyUtilsNew().stringToDate(data[0].dateEvent)
        //home
        home_name.text = data[0].strHomeTeam
        home_score.text = data[0].intHomeScore
        home_formation.text = data[0].strHomeFormation
        home_goal_keeper.text = data[0].strHomeLineupGoalkeeper
        home_shots.text = data[0].intHomeShots

        swipeRefresh.isRefreshing = false

        detailEventModel = DetailEventModel(data[0].idEvent, data[0].dateEvent,
                data[0].strHomeTeam, data[0].strAwayTeam, data[0].intHomeScore,
                data[0].intAwayScore, data[0].strHomeGoalDetails, data[0].strHomeLineupGoalkeeper,
                data[0].strHomeLineupDefense, data[0].strHomeLineupMidfield, data[0].strHomeLineupForward,
                data[0].strHomeLineupSubstitutes, data[0].strHomeFormation, data[0].strAwayGoalDetails,
                data[0].strAwayLineupGoalkeeper, data[0].strAwayLineupDefense, data[0].strAwayLineupMidfield,
                data[0].strAwayLineupForward, data[0].strAwayLineupSubstitutes, data[0].strAwayFormation,
                data[0].intHomeShots, data[0].intAwayShots, data[0].idHomeTeam, data[0].idAwayTeam, data[0].strTime)

        if (!data[0].strAwayLineupDefense.isNullOrEmpty()) {
            //home
            home_goals.text = loopSplit(data[0].strHomeGoalDetails, ";")
            home_mid.text = loopSplit(data[0].strHomeLineupMidfield, "; ")
            home_defense.text = loopSplit(data[0].strHomeLineupDefense, "; ")
            home_fordward.text = loopSplit(data[0].strHomeLineupForward, "; ")
            home_subtitutes.text = loopSplit(data[0].strHomeLineupSubstitutes, "; ")

            //away
            away_goals.text = loopSplit(data[0].strAwayGoalDetails, ";")
            away_mid.text = loopSplit(data[0].strAwayLineupMidfield, "; ")
            away_defense.text = loopSplit(data[0].strAwayLineupDefense, "; ")
            away_fordward.text = loopSplit(data[0].strAwayLineupForward, "; ")
            away_subtitutes.text = loopSplit(data[0].strAwayLineupSubstitutes, "; ")


        }
        

        //away
        away_name.text = data[0].strAwayTeam
        away_score.text = data[0].intAwayScore
        away_formation.text = data[0].strAwayFormation
        away_goal_keeper.text = data[0].strAwayLineupGoalkeeper
        away_shots.text = data[0].intAwayShots

        vs.visibility = View.VISIBLE

        presenter.getDetailClub1(data[0].idHomeTeam)
        presenter.getDetailClub2(data[0].idAwayTeam)

        detail_toolbar?.title = resources.getString(match_detail)
        EspressoIdlingResource.decrement()

    }


    private fun loopSplit(data: String?, mark: String): String{
        var dataGoal: String = ""
        val dataSplit = data!!.split(mark)
        var i = 1
        for (item: String in dataSplit){
            if (i != dataSplit.size){dataGoal = dataGoal + item +";\n"}
            i++
        }

        return dataGoal
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(detail_toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        swipeRefresh.isEnabled = false

        val request = ApiRepository()
        val gson = Gson()
        idEvent = intent.getStringExtra(resources.getString(id_event))
        presenter = DetailPresenter(this, request, gson)
        presenter.getDetail(resources.getString(lookup_event), idEvent)
        EspressoIdlingResource.increment()

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            super.onBackPressed()
        }
        when(item.itemId){
            android.R.id.home ->{
                super.onBackPressed()
            }

            add_to_favorite -> {
                if (isFavorite)removeFromFav() else addToFav()

                isFavorite = !isFavorite
                setFavorite()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        checkFavorite()
        return true
    }

    private fun addToFav(){
        try {
            database.use{
                insert(FavoriteMatch.TB_EVENT_FAVORITE,
                        FavoriteMatch.ID_EVENT to detailEventModel.idEvent,
                        FavoriteMatch.EVENT_DATE to detailEventModel.dateEvent,
                        FavoriteMatch.TIME_EVENT to detailEventModel.strTime,
                        FavoriteMatch.HOME_TEAM to detailEventModel.strHomeTeam,
                        FavoriteMatch.HOME_SCORE to detailEventModel.intHomeScore,
                        FavoriteMatch.AWAY_TEAM to detailEventModel.strAwayTeam,
                        FavoriteMatch.AWAY_SCORE to detailEventModel.intAwayScore)

                snackbar(swipeRefresh, resources.getString(changed)).show()
            }
        }catch (e: SQLiteConstraintException){
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun removeFromFav(){
        try {
            database.use {
                delete(FavoriteMatch.TB_EVENT_FAVORITE,
                        "(EVENT_ID = {id})",
                        resources.getString(idOnly) to idEvent)
            }

            snackbar(swipeRefresh, resources.getString(changed)).show()
        }catch (e: SQLiteConstraintException){
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
        {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_added_to_favorites)
        }
        else{
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_add_to_favorites)
        }

    }

    private fun checkFavorite(){
        database.use {
            val result = select(FavoriteMatch.TB_EVENT_FAVORITE)
                    .whereArgs(FavoriteMatch.ID_EVENT +" = {idEvent}",
                            resources.getString(id_event) to intent.getStringExtra(resources.getString(id_event)))
            val favorite = result.parseList(classParser<FavoriteMatch>())
            if (favorite.count()>0){
                isFavorite = true
                setFavorite()
            }

        }
    }
}
