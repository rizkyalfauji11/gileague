package com.acs.rizky.gileague.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.color.white
import com.acs.rizky.gileague.R.id.team_badge
import com.acs.rizky.gileague.R.id.team_name
import com.acs.rizky.gileague.model.Team
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView


class TeamsAdapter(private var teams: MutableList<Team>, private val listener:(Team)->Unit)
    : RecyclerView.Adapter<TeamViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        return TeamViewHolder(TeamUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(teams[position], listener)
    }

    fun setFilter(filterList: MutableList<Team>) {
        teams = mutableListOf()
        teams.addAll(filterList)
        notifyDataSetChanged()
    }

}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){
    private val teamBadge: ImageView = view.find(team_badge)
    private val teamName: TextView = view.find(team_name)
    fun bindItem(teams: Team, listener: (Team) -> Unit){
        Picasso.get().load(teams.teamBagde).into(teamBadge)
        teamName.text = teams.teamName
        itemView.setOnClickListener {
            listener(teams)
        }
    }
}



class TeamUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui){
            cardView {
                cardElevation = 2F
                lparams(width = matchParent)
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(16)
                    orientation = LinearLayout.HORIZONTAL

                    imageView {
                        id = R.id.team_badge
                    }.lparams {
                        height = dip(50)
                        width = dip(50)
                    }

                    textView {
                        id = R.id.team_name
                        textSize = 16F
                    }.lparams {
                        margin = dip(15)
                    }
                }
            }
        }

    }
}