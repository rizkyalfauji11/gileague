package com.acs.rizky.gileague.presenter

import com.acs.rizky.gileague.api_response.TeamResponse
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.acs.rizky.gileague.view.TeamView
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async

import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.custom.async

class TeamsPresenter(private val view: TeamView,
                     private val apiRespository: ApiRepository,
                     private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getTeamList(league: String?){
        view.showLoading()
        /*doAsync {
            val data = gson.fromJson(apiRespository
                    .doRequest(SportDBApi.getTeams(league)),
                    TeamResponse::class.java
            )
            println("GSON: "+gson.toString())
            uiThread {
                view.hideLoading()
                view.showTeamList(data.teams)
            }
        }*/
        async(context.main){
            val data = bg {
                gson.fromJson(apiRespository.doRequest(SportDBApi.getTeams(league)),
                        TeamResponse::class.java
                )
            }

            view.showTeamList(data.await().teams)
            view.hideLoading()
        }
    }
}