package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.Team

data class  TeamResponse(
        val teams: List<Team>
)