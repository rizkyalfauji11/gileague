package com.acs.rizky.gileague.model

import com.google.gson.annotations.SerializedName

data class ScheduleModel (
    @SerializedName("strHomeTeam")
    var strHomeTeam: String? = null,

    @SerializedName("strAwayTeam")
    var strAwayTeam: String? = null,

    @SerializedName("dateEvent")
    var dateEvent: String? = null,

    @SerializedName("intHomeScore")
    var intHomeScore: String? = null,

    @SerializedName("intAwayScore")
    var intAwayScore: String? = null,

    @SerializedName("strTime")
    var strTime: String? = null,

    @SerializedName("strFilename")
    var strFilename: String? = null,

    @SerializedName("idEvent")
    var idEvent: String? = null
)