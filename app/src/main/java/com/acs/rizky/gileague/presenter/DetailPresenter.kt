package com.acs.rizky.gileague.presenter

import com.acs.rizky.gileague.api_response.DetailClubResponse1
import com.acs.rizky.gileague.api_response.DetailClubResponse2
import com.acs.rizky.gileague.api_response.DetailResponse
import com.acs.rizky.gileague.view.DetailView
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetailPresenter(private val view: DetailView,
                      private val api: ApiRepository,
                      private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()){
    fun getDetail(league: String?, id: String?){
        view.showLoading()

        async(context.main) {
            val data = bg {
                gson.fromJson(api
                        .doRequest(SportDBApi.getDetail(league, id)),
                        DetailResponse::class.java
                )
            }
            view.showDetailView(data.await().events)
            view.hideLoading()

        }
    }

    fun getDetailClub1(id: String?){
        async(context.main){
            val data = bg {
                gson.fromJson(api
                        .doRequest(SportDBApi.getDetailClub(id)),
                        DetailClubResponse1::class.java)
            }
                view.showDetailClub1(data.await().teams)
        }
    }

    fun getDetailClub2(id: String?){
        async(context.main) {

            val data = bg {
                gson.fromJson(api
                        .doRequest(SportDBApi.getDetailClub(id)),
                        DetailClubResponse2::class.java)
            }
                view.showDetailClub2(data.await().teams)
        }

    }
}