package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.DetailPlayerModel
import com.acs.rizky.gileague.model.PlayerListModel

data class PlayerDetailResponse(
        val players: List<DetailPlayerModel>
)