package com.acs.rizky.gileague.utilities

import android.annotation.SuppressLint
import kotlinx.android.synthetic.main.activity_detail.*
import java.text.SimpleDateFormat
import java.util.*

class MyUtilsNew {
    @SuppressLint("SimpleDateFormat")
    fun stringToDate(dates: String?) : String{
        try {
            val originalFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val targetFormat = SimpleDateFormat("EEE, dd MMM yyyy")
            val date = originalFormat.parse(dates)
            return targetFormat.format(date).toString()

        }catch (e: Exception){
            e.printStackTrace()
        }

        return ""

    }

}