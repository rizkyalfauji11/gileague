package com.acs.rizky.gileague.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.model.PlayerListModel
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.w3c.dom.Text

class PlayersAdapter(private val context: Context, private val players: List<PlayerListModel>,
                     private val listener: (PlayerListModel) -> Unit)
    : RecyclerView.Adapter<PlayerViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        PlayerViewHolder(LayoutInflater.from(context).inflate(R.layout.player_list_item, parent, false))

    override fun getItemCount(): Int = players.size

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(players[position], listener)
    }

}

class PlayerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val image = view.findViewById<CircleImageView>(R.id.image)
    private val name = view.findViewById<TextView>(R.id.name)
    private val position = view.findViewById<TextView>(R.id.position)

    fun bindItem(players: PlayerListModel, listener: (PlayerListModel) -> Unit){
        Picasso.get().load(players.strCutout).into(image)
        name.text = players.strPlayer
        position.text = players.strPosition
        itemView.setOnClickListener {
            listener(players)
        }
    }
}
