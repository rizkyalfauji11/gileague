package com.acs.rizky.gileague.model

import com.google.gson.annotations.SerializedName

data class DetailClubModel1(
        @SerializedName("strTeamBadge")
        var strTeamBadge: String? = null
)