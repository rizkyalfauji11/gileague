package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.DetailPlayerModel


interface DetailPlayerView{
    fun showLoading()
    fun hideLoading()
    fun showDetailPlayer(data: List<DetailPlayerModel>)
}