package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.Team

interface TeamView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}