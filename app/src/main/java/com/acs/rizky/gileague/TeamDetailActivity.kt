package com.acs.rizky.gileague

import android.database.sqlite.SQLiteConstraintException
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.acs.rizky.gileague.R.drawable.ic_add_to_favorites
import com.acs.rizky.gileague.R.drawable.ic_added_to_favorites
import com.acs.rizky.gileague.R.id.add_to_favorite
import com.acs.rizky.gileague.R.menu.detail_menu
import com.acs.rizky.gileague.R.string.idOnly
import com.acs.rizky.gileague.R.string.overview
import com.acs.rizky.gileague.adapter.TabMatchAdapter
import com.acs.rizky.gileague.fragment.OverviewTeamFragment
import com.acs.rizky.gileague.fragment.TeamPlayerFragment
import com.acs.rizky.gileague.mydb.Favorite
import com.acs.rizky.gileague.mydb.database
import com.acs.rizky.gileague.model.Team
import com.acs.rizky.gileague.mydb.FavoriteMatch
import com.acs.rizky.gileague.presenter.TeamDetailPresenter
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.EspressoIdlingResource
import com.acs.rizky.gileague.view.TeamDetailView
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_detail.*
import kotlinx.coroutines.experimental.selects.select
import org.jetbrains.anko.db.classParser

import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar

class TeamDetailActivity : AppCompatActivity(), TeamDetailView {
    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showTeamDetail(data: List<Team>) {
        teams = Team(data[0].teamId,
                data[0].teamName,
                data[0].teamBagde)
        swipeRefresh.isRefreshing = false
        Picasso.get().load(data[0].teamBagde).into(teamBadge)
        teamName.text = data[0].teamName
        //
        teamFormedYear.text = data[0].teamFormedYear
        teamStadium.text = data[0].teamStadium
        addTab(data[0].teamDescription)
        EspressoIdlingResource.decrement()
    }

    private lateinit var presenter: TeamDetailPresenter
    private lateinit var teams: Team
    private lateinit var id: String

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_detail)
        setSupportActionBar(detail_toolbar)
        supportActionBar?.title = "Team Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        swipeRefresh.isEnabled = false
        val intent = intent
        id = intent.getStringExtra("id")
        val request = ApiRepository()
        val gson = Gson()
        presenter = TeamDetailPresenter(this, request, gson)
        EspressoIdlingResource.increment()
        presenter.getTeamDetail(id)
    }

    private fun addTab(desc: String?) {
        val tabMatchAdapter = TabMatchAdapter(supportFragmentManager)
        tabMatchAdapter.add(OverviewTeamFragment.newInstance(desc), resources.getString(overview))
        tabMatchAdapter.add(TeamPlayerFragment.newInstance(teams.teamId), "PLAYER")
        detail_viewpager.adapter = tabMatchAdapter
        detail_tabs.setupWithViewPager(detail_viewpager)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu, menu)
        menuItem = menu
        checkFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()

                isFavorite = !isFavorite
                setFavorite()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun addToFavorite(){
        try{
            database.use{
                insert(Favorite.TABLE_FAVORITE,
                        Favorite.TEAM_ID to teams.teamId,
                        Favorite.TEAM_NAME to teams.teamName,
                        Favorite.TEAM_BADGE to teams.teamBagde)

                snackbar(swipeRefresh, "Changed").show()
            }
        }catch (e: SQLiteConstraintException){
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite(){
        try {
            database.use {
                delete(Favorite.TABLE_FAVORITE, "(TEAM_ID = {id})",
                        "id" to id)
            }
            snackbar(swipeRefresh, "Changed").show()
        } catch (e: SQLiteConstraintException){
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    private fun checkFavorite(){
        database.use {
            val result = select(Favorite.TABLE_FAVORITE)
                    .whereArgs(Favorite.TEAM_ID +" = {id}",
                            resources.getString(idOnly) to id)
            val favorite = result.parseList(classParser<Favorite>())
            if (favorite.count()>0){
                isFavorite = true
                setFavorite()
            }

        }
    }
}
