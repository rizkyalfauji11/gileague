package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.LeagueListModel
import com.acs.rizky.gileague.model.ScheduleModel

interface ScheduleView {
    fun showLoading()
    fun hideLoading()
    fun showScheduleList(data: List<ScheduleModel>)

}