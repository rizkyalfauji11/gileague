package com.acs.rizky.gileague.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.string.match
import com.acs.rizky.gileague.R.string.teams
import com.acs.rizky.gileague.adapter.TabMatchAdapter
import kotlinx.android.synthetic.main.fragment_tab_team.*

class TabFavoriteFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab_team, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)
        addTab()
    }

    private fun addTab() {
        val tabMatchAdapter = TabMatchAdapter(childFragmentManager)
        tabMatchAdapter.add(FavoriteMatchFragment(), resources.getString(match))
        tabMatchAdapter.add(FavoriteTeamsFragment(), resources.getString(teams))
        viewpager.adapter = tabMatchAdapter
        tabs.setupWithViewPager(viewpager)
    }
}
