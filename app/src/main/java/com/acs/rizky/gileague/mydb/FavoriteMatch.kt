package com.acs.rizky.gileague.mydb

data class FavoriteMatch(val id:Long?, val idEvent: String?, val eventDate: String?, val homeTeam: String?,
                         val homeScore: String?, val awayTeam: String?, val awayScore: String?, val eventTime: String?){
    companion object {
        const val TB_EVENT_FAVORITE = "TABLE_EVENT_FAVORITE"
        const val ID_TB_EVENT = "ID_"
        const val ID_EVENT = "EVENT_ID"
        const val EVENT_DATE = "EVENT_DATE"
        const val HOME_TEAM = "HOME_TEAM"
        const val AWAY_TEAM = "AWAY_TEAM"
        const val HOME_SCORE = "HOME_SCORE"
        const val AWAY_SCORE = "AWAY_SCORE"
        const val TIME_EVENT = "TIME_EVENT"

    }
}