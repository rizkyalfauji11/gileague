package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.DetailClubModel1
import com.acs.rizky.gileague.model.DetailClubModel2
import com.acs.rizky.gileague.model.DetailEventModel

interface DetailView {
    fun showLoading()
    fun hideLoading()
    fun showDetailView(data: List<DetailEventModel>)
    fun showDetailClub1(data: List<DetailClubModel1>)
    fun showDetailClub2(data: List<DetailClubModel2>)
}