package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.DetailClubModel1

data class DetailClubResponse1(
        val teams: List<DetailClubModel1>
)