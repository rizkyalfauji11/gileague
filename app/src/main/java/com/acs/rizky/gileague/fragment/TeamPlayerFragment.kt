package com.acs.rizky.gileague.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.acs.rizky.gileague.DetailActivity
import com.acs.rizky.gileague.DetailPlayerActivity

import com.acs.rizky.gileague.R
import com.acs.rizky.gileague.R.string.idOnly
import com.acs.rizky.gileague.R.string.id_event
import com.acs.rizky.gileague.adapter.PlayersAdapter
import com.acs.rizky.gileague.model.PlayerListModel
import com.acs.rizky.gileague.model.Team
import com.acs.rizky.gileague.presenter.PlayerPresenter
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.DividerItemDecoration
import com.acs.rizky.gileague.utilities.EspressoIdlingResource
import com.acs.rizky.gileague.view.PlayerView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_team_player.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.toast


class TeamPlayerFragment : Fragment(), PlayerView {

    companion object {
        fun newInstance(text: String?) : TeamPlayerFragment{
            val fragment = TeamPlayerFragment()
            val args = Bundle()
            args.putString("id", text)
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var presenter: PlayerPresenter
    private var players: MutableList<PlayerListModel> = mutableListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_team_player, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rv.layoutManager = LinearLayoutManager(ctx)
        rv.addItemDecoration(DividerItemDecoration(ctx))
        rv.adapter = PlayersAdapter(ctx,players){
            ctx.startActivity<DetailPlayerActivity>(resources.getString(idOnly) to it.idPlayer)
        }

        EspressoIdlingResource.increment()
        val request = ApiRepository()
        val gson = Gson()
        presenter = PlayerPresenter(this, request, gson)
        presenter.getPlayers(arguments?.getString("id"))

    }

    override fun showListPlayer(data: List<PlayerListModel>) {
        players.clear()
        players.addAll(data)
        rv.adapter.notifyDataSetChanged()
        EspressoIdlingResource.decrement()
    }
}
