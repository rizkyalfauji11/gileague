package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.PlayerListModel

data class PlayerListResponse(
        val player: List<PlayerListModel>
)