package com.acs.rizky.gileague.presenter


import com.acs.rizky.gileague.api_response.PlayerDetailResponse
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.acs.rizky.gileague.view.DetailPlayerView
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerDetailPresenter(private val view: DetailPlayerView,
                      private val api: ApiRepository,
                      private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()){
    fun getDetailPlayer(id: String?){
        view.showLoading()

        async(context.main) {
            val data = bg {
                gson.fromJson(api
                        .doRequest(SportDBApi.getDetailPlayer(id)),
                        PlayerDetailResponse::class.java
                )
            }
            view.showDetailPlayer(data.await().players)
            view.hideLoading()

        }
    }

}
