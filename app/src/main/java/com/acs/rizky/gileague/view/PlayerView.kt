package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.PlayerListModel

interface PlayerView{
    fun showListPlayer(data: List<PlayerListModel>)
}