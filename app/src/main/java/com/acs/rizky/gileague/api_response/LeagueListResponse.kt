package com.acs.rizky.gileague.api_response

import com.acs.rizky.gileague.model.LeagueListModel

data class LeagueListResponse(
        val leagues: List<LeagueListModel>
)