package com.acs.rizky.gileague.model

import com.google.gson.annotations.SerializedName

data class DetailPlayerModel(
        @SerializedName("strPlayer")
        var strPlayer: String? = null,

        @SerializedName("strPosition")
        var strPosition: String? = null,

        @SerializedName("strHeight")
        var strHeight: String? = null,

        @SerializedName("strWeight")
        var strWeight: String? = null,

        @SerializedName("strDescriptionEN")
        var strDescriptionEN: String? = null,

        @SerializedName("strFanart2")
        var strFanart2: String? = null

)