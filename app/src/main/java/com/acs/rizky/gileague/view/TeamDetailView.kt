package com.acs.rizky.gileague.view

import com.acs.rizky.gileague.model.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun showTeamDetail(data: List<Team>)
}