package com.acs.rizky.gileague.model

import com.google.gson.annotations.SerializedName

data class PlayerListModel(
        @SerializedName("idPlayer")
        var idPlayer: String? = null,

        @SerializedName("strPlayer")
        var strPlayer: String? = null,

        @SerializedName("strPosition")
        var strPosition: String? = null,

        @SerializedName("strCutout")
        var strCutout: String? = null
)