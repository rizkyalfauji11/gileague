package com.acs.rizky.gileague.utilities

import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat
import java.util.*

class UtilsKtTest {

    @Test
    fun testToSimpleString() {
        val dateFormat = SimpleDateFormat("MM/dd/yyyy")
        val date = dateFormat.parse("02/28/2018")
        assertEquals("Wed, 28 Feb 2018", toSimpleString(date))
    }

    fun toSimpleString(date: Date?):String? = with(date ?: Date()){
        SimpleDateFormat("EEE, dd MMM yyyy").format(this)
    }
}