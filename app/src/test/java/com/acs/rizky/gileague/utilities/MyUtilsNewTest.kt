package com.acs.rizky.gileague.utilities

import android.annotation.SuppressLint
import org.junit.Test

import org.junit.Assert.*
import java.text.SimpleDateFormat
import java.util.*

class MyUtilsNewTest {

    @Test
    fun stringToDate() {
        assertEquals("Wed, 28 Feb 2018", stringToDate("2018-02-28"))
    }

    @SuppressLint("SimpleDateFormat")
    fun stringToDate(dates: String?) : String{
        try {
            val originalFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val targetFormat = SimpleDateFormat("EEE, dd MMM yyyy")
            val date = originalFormat.parse(dates)
            return targetFormat.format(date).toString()

        }catch (e: Exception){
            e.printStackTrace()
        }

        return ""

    }
}