package com.acs.rizky.gileague.presenter

import com.acs.rizky.gileague.api_response.ScheduleResponse
import com.acs.rizky.gileague.model.ScheduleModel
import com.acs.rizky.gileague.utilities.ApiRepository
import com.acs.rizky.gileague.utilities.SportDBApi
import com.acs.rizky.gileague.view.ScheduleView
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class SchedulesPresenterTest{
    @Mock
    private
    lateinit var view: ScheduleView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: SchedulesPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SchedulesPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testGetScheduleList(){
        val schedules: MutableList<ScheduleModel> = mutableListOf()
        val response = ScheduleResponse(schedules)
        val event_past_league = "eventspastleague"
        val id_league = "4328"

         `when`(gson.fromJson(apiRepository.doRequest(SportDBApi.getSchedule(event_past_league, id_league)),
                ScheduleResponse::class.java
        )).thenReturn(response)

        presenter.getScheduleList(event_past_league, id_league)

        verify(view).showLoading()
        verify(view).showScheduleList(schedules)
        verify(view).hideLoading()


    }
}