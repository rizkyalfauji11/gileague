package com.acs.rizky.gileague

import android.support.design.widget.TabLayout
import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onData
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.acs.rizky.gileague.R.id.*
import com.acs.rizky.gileague.utilities.EspressoIdlingResource
import org.hamcrest.CoreMatchers.allOf
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import android.os.SystemClock
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA



@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest{

    @Rule
    @JvmField var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp(){
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource);
    }

    @Test
    fun testRecyclerViewBehaviour() {

        val matcher = allOf(withText("NEXT"),
                isDescendantOfA(withId(R.id.tabs)))
        onView(matcher).perform(click())
        SystemClock.sleep(800)

        Espresso.onView(ViewMatchers.withId(rv_2))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(rv_2)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.rv_2)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        onView(withId(R.id.add_to_favorite))
                .check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        onView(withText("Changed"))
                .check(matches(isDisplayed()))

        Espresso.pressBack()

        onView(withId(navigationView))
                .check(matches(isDisplayed()))
        onView(withId(next_match)).perform(click())


        Espresso.onView(ViewMatchers.withId(list_event))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        Espresso.onView(ViewMatchers.withId(list_event)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.list_event)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        onView(withId(R.id.add_to_favorite))
                .check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        onView(withText("Changed"))
                .check(matches(isDisplayed()))

        Espresso.pressBack()

        onView(withId(navigationView))
                .check(matches(isDisplayed()))
        onView(withId(favorites)).perform(click())

        Espresso.onView(ViewMatchers.withId(rv_1))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(withId(R.id.rv_1)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        onView(withId(R.id.add_to_favorite))
                .check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        onView(withText("Changed"))
                .check(matches(isDisplayed()))

        Espresso.pressBack()

        onView(withId(navigationView))
                .check(matches(isDisplayed()))
        onView(withId(favorites)).perform(click())

        val matcher2 = allOf(withText("Teams"),
                isDescendantOfA(withId(R.id.tabs)))
        onView(matcher2).perform(click())
        SystemClock.sleep(800)

    }
}